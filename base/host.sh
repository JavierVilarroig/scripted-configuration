#!/usr/bin/env bash

set -ex

function host_configure_container () {
	if [ -z "${CPU+x}" ]; then
		read -p "How many CPU cores should be made available? " CPU
		: ${CPU:=2}
	fi

	if [ -z "${MEM+x}" ]; then
		read -p "How much memory should be made available, in GB? " MEM
		: ${MEM:=2}
	fi

	MEM_MAX=$((1024**3 * ${MEM}))
	CPU_MAX=$((100000 * ${CPU}))
	: ${WEIGHT:=80} # slightly reduce CPU weight by default to keep host responsive, 100 is normal
	: ${CPU_WEIGHT:=${WEIGHT}}
	: ${IO_WEIGHT:=${WEIGHT}}
	: ${VIRTUALIZATION:=lxc}
}

function setup_new_kvm () {
        script_dir=$(dirname "$(realpath "${BASH_SOURCE[0]}")")
        tmp_name="debian-preseed-$(date "+%s")"
        mkdir /tmp/${tmp_name}
        chmod 700 -R /tmp/${tmp_name}
        cp ${script_dir}/preseed.cfg /tmp/${tmp_name}/preseed.cfg
        cp ${script_dir}/postinst.sh /tmp/${tmp_name}/postinst.sh
        echo "d-i netcfg/get_hostname string ${HOSTNAME}" >> /tmp/${tmp_name}/preseed.cfg

	virt-install --virt-type kvm --name "${HOSTNAME}" \
		--location http://deb.debian.org/debian/dists/bookworm/main/installer-amd64/ \
		--os-variant debiantesting \
		--memory $((MEM * 1024)) \
		--graphics none \
                --initrd-inject=/tmp/${tmp_name}/preseed.cfg \
                --initrd-inject=/tmp/${tmp_name}/postinst.sh \
		--console pty,target_type=serial \
		--extra-args "console=ttyS0" \
		--vcpus ${CPU} \
		--disk size=100 \
		--network=default

        rm -rf /tmp/${tmp_name}
	virsh autostart "${HOSTNAME}"
}

function setup_new_lxc () {
	LXCSTORAGE="/var/lib/lxc"

	if [ "$EUID" -ne 0 ]; then
		if ! confirm "You are executing this script as '$USER', containers will not be global, but user specific. Do you want to continue?"; then exit 1; fi
		echo "Running as User '$USER', checking requirements"
		userpasswd="$(getent passwd "$USER" || (echo >&2 "failed to get passwd entry for user $USER while resolving home directory" && exit 1))"
		USERHOME="$(echo "$userpasswd" | cut -d: -f6)"
		LXCSTORAGE="${USERHOME}/.local/share/lxc"
		LXCDEFAULTCONFIG="${USERHOME}/.config/lxc/default.conf"
		echo "Running in user mode, lxc-storage is '$LXCSTORAGE'"
		if ! [ -d "${LXCSTORAGE}" ] || ! [ -f "${LXCDEFAULTCONFIG}" ]; then
			echo "user based lxc not configured"
			exit 1
		fi
	fi

	# create container if not exists
	if [ ! -d "${LXCSTORAGE}/${HOSTNAME}" ]; then
		lxc-create --name="${HOSTNAME}" --template=debian -- --release bookworm
		rsync . "${LXCSTORAGE}/${HOSTNAME}/rootfs/root/scripted-configuration.git" -aR
		git remote add "${HOSTNAME}" "${LXCSTORAGE}/${HOSTNAME}/rootfs/root/scripted-configuration.git"
	fi

	# clear per-service config from file
	sed -i '/# per-service config starts here/,$d' "${LXCSTORAGE}/${HOSTNAME}/config"
	envsubst <<EOF >> "${LXCSTORAGE}/${HOSTNAME}/config"

# per-service config starts here
# don't manually edit below, will be overriden
lxc.start.auto = 1
lxc.cgroup2.memory.max = ${MEM_MAX}
lxc.cgroup2.cpu.max = ${CPU_MAX}
lxc.cgroup2.cpu.weight = ${CPU_WEIGHT}
lxc.cgroup2.io.weight = ${CPU_WEIGHT}
lxc.cgroup2.devices.allow = c 10:229 rwm
${LXC_CUSTOM}
EOF

	if confirm "Do you want to (re-)start container now?"; then
		lxc-stop "${HOSTNAME}" || if [ $? == 2 ]; then true; fi
		lxc-start "${HOSTNAME}"
	fi
	sleep 5 # wait for network to come up (workaround)

	if [ -f "${LXCSTORAGE}/${HOSTNAME}/rootfs/root/scripted-configuration.git/inside.sh" ]; then
		lxc-attach --name="${HOSTNAME}" -- /root/scripted-configuration.git/inside.sh
	fi
}
