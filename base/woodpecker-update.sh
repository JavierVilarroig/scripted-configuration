#!/bin/bash

set -x

if [[ ${#@} -ne 1 ]]; then
    echo "the woodpecker docker tag must be provided"
    exit 1
fi

TAG="${1}"

BINLOC="$WPBASEDIR/wp-bins"
SERVEROUT="${BINLOC}/woodpecker-server-out"
AGENTOUT="${BINLOC}/woodpecker-agent-out"

ACTIVEBIN="$WPBASEDIR/bin"

test -f "${SERVEROUT}" && rm "${SERVEROUT}"
test -f "${AGENTOUT}" && rm "${AGENTOUT}"

SERVER_IMAGE="woodpeckerci/woodpecker-server:${TAG}"
AGENT_IMAGE="woodpeckerci/woodpecker-agent:${TAG}"

echo "pulling server"
SERVER_ID=$(docker create --name server-image-fetcher "${SERVER_IMAGE}")
docker cp "${SERVER_ID}:/bin/woodpecker-server" "${SERVEROUT}"
docker rm server-image-fetcher

echo "pulling agent"
AGENT_ID=$(docker create --name agent-image-fetcher "${AGENT_IMAGE}")
docker cp "${AGENT_ID}:/bin/woodpecker-agent" "${AGENTOUT}"
docker rm agent-image-fetcher

echo "verifying"
if [[ ! -f "${SERVEROUT}" ]]; then
    echo "Error retreiving woodpecker-server binary" >&2
    exit 1
fi

if [[ ! -f "${AGENTOUT}" ]]; then
    echo "Error retreiving woodpecker-agent binary" >&2
    exit 1
fi

if ! "${SERVEROUT}" --version | grep -q 'version'; then
    echo "Error woodpecker-server binary not showing version" >&2
    exit 1
fi

if ! "${AGENTOUT}" --version | grep -q 'version'; then
    echo "Error woodpecker-agent binary not showing version" >&2
    exit 1
fi

SERVERVERSION="$("${SERVEROUT}" --version | rev | cut -d' ' -f1 | rev | tr -d "[:space:]")"
AGENTVERSION="$("${AGENTOUT}" --version | rev | cut -d' ' -f1 | rev | tr -d "[:space:]")"

echo "Moving to version suffix"
mv "${SERVEROUT}" "${BINLOC}/woodpecker-server-${SERVERVERSION}"
mv "${AGENTOUT}" "${BINLOC}/woodpecker-agent-${AGENTVERSION}"

echo "Updating server"
test -f "${ACTIVEBIN}/woodpecker-server-prev" && rm "${ACTIVEBIN}/woodpecker-server-prev"
test -f "${ACTIVEBIN}/woodpecker-server" && mv "${ACTIVEBIN}/woodpecker-server" "${ACTIVEBIN}/woodpecker-server-prev" && PREVSERVER=true
cp "${BINLOC}/woodpecker-server-${SERVERVERSION}" "${ACTIVEBIN}/woodpecker-server"

echo "Updating agent"
test -f "${ACTIVEBIN}/woodpecker-agent-prev" && rm "${ACTIVEBIN}/woodpecker-agent-prev"
test -f "${ACTIVEBIN}/woodpecker-agent" && mv "${ACTIVEBIN}/woodpecker-agent" "${ACTIVEBIN}/woodpecker-agent-prev" && PREVAGENT=true
cp "${BINLOC}/woodpecker-agent-${AGENTVERSION}" "${ACTIVEBIN}/woodpecker-agent"

test -n "${PREVSERVER}" && PREVSERVERVERSION="$("${ACTIVEBIN}/woodpecker-server-prev" --version | rev | cut -d' ' -f1 | rev | tr -d "[:space:]")"
test -n "${PREVAGENT}" && PREVAGENTVERSION="$("${ACTIVEBIN}/woodpecker-agent-prev" --version | rev | cut -d' ' -f1 | rev | tr -d "[:space:]")"

echo "setting permissions and ownership"
chown -R root:woodpecker-shared "${ACTIVEBIN}/"
chmod 750 "${ACTIVEBIN}/woodpecker-server" "${ACTIVEBIN}/woodpecker-agent"

echo "$(date) Updated server from ${PREVSERVERVERSION:-none} to ${SERVERVERSION} (tag: $TAG)" | tee -a "$WPBASEDIR/update.log"
echo "$(date) Updated agent from ${PREVAGENTVERSION:-none} to ${AGENTVERSION}  (tag: $TAG)" | tee -a "$WPBASEDIR/update.log"
echo "To apply the changed binaries restart woordpecker-server and woodpecker-agent"
