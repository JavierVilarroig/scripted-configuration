#!/usr/bin/env bash

set -ex

source "base/users.sh"

user_setup "otto" "sudo"
user_setup "tobiasfichtner" "sudo"
user_setup "ashimokawa" "sudo"

source "base/base.sh"
setup_sshd
