#!/usr/bin/env bash

set -ex

source "base/users.sh"
source "base/secrets.sh"

user_setup "otto" "sudo"
user_setup "ashimokawa" "sudo"
user_setup "reg-server"

source "base/base.sh"
setup_sshd

user_grant "root" "otto"
user_grant "root" "ashimokawa"

# Mail
DEBIAN_FRONTEND=noninteractive apt-get install -y --no-install-recommends postfix

secret_get "SMTP_PASSWORD"
secret_get "POLLS_PASSWORD_ADMIN"
secret_get "POLLS_PASSWORD_MEMBER"
secret_get "MARIADB_VOTING"

sed -i '/relayhost =/d' /etc/postfix/main.cf # always remove old relayhost setting
sed -i '/inet_interfaces =/d' /etc/postfix/main.cf # always remove old inet_interfaces setting
line_in_file /etc/postfix/main.cf "smtp_sasl_auth_enable = yes"
line_in_file /etc/postfix/main.cf "smtp_sasl_password_maps = static:gitea:${SMTP_PASSWORD}"
line_in_file /etc/postfix/main.cf "smtp_sasl_security_options = noanonymous"
line_in_file /etc/postfix/main.cf "relayhost = smtp.codeberg.org"
line_in_file /etc/postfix/main.cf "inet_interfaces = 127.0.0.1,::1"

echo "address { email-domain codeberg.org; };" > /etc/mailutils.conf
echo "codeberg.org" > /etc/mailname

systemctl restart postfix


# Voting
DEST_DIR=/var/www/polls
apt-get install -y --no-install-recommends nginx-light php-fpm php-mysql rsync apache2-utils

touch /etc/nginx/sites-available/polls # workaround for a bug of install_file which assumes it to be there already
install_file "ev" "/etc/nginx/sites-available/polls"

htpasswd -b -c /etc/nginx/.htpasswd admin ${POLLS_PASSWORD_ADMIN}
htpasswd -b -c /etc/nginx/.htpasswd_users member ${POLLS_PASSWORD_MEMBER}
chown www-data.www-data /etc/nginx/.htpasswd*
chmod 640 /etc/nginx/.htpasswd*

if [ ! -f /etc/nginx/sites-enabled/polls ]; then
	ln -s /etc/nginx/sites-available/polls /etc/nginx/sites-enabled/polls
fi

rm -f /etc/nginx/sites-enabled/default

cd /root
if [ -d codeberg_voting_system.git ]; then
        cd codeberg_voting_system.git
        git pull origin main
else
        git clone https://codeberg.org/Codeberg/codeberg_voting_system.git codeberg_voting_system.git
        cd codeberg_voting_system.git
fi

rsync -av -e ssh --delete --exclude '*~' --exclude .git --exclude config.inc.local.php src/ ${DEST_DIR}

DEST_FILE="${DEST_DIR}/config.inc.local.php"
echo "<?php" > ${DEST_FILE}
echo "const DBPASSWD='${MARIADB_VOTING}';" >> ${DEST_FILE}
echo "\$polls_url = 'https://member:${POLLS_PASSWORD_MEMBER}@polls.codeberg.org/';" >>  ${DEST_FILE}
echo "\$mysql_server = 'mariadb.lxc.local';" >>  ${DEST_FILE}

chown -R www-data:www-data ${DEST_DIR}

systemctl restart nginx

# reg-server

apt-get install -y --no-install-recommends gnupg mailutils


# Firewall

apt-get install -y --no-install-recommends ufw
ufw enable
ufw allow from 10.0.3.1 # allow host only
ufw default reject
