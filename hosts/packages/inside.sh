#!/usr/bin/env bash

set -ex

source "base/users.sh"

user_setup "otto" "sudo"
user_setup "ashimokawa" "sudo"
user_setup "izzy"


source "base/base.sh"
setup_sshd

apt-get install -y --no-install-recommends lighttpd rsync

rm -f /var/www/html/index.lighttpd.html
lighty-enable-mod userdir || true

function create_public_html () {
	USERNAME="${1}"
	chown "${USERNAME}:www-data" "/home/${USERNAME}"
	chmod 750 "/home/${USERNAME}"

	mkdir -p "/home/${USERNAME}/public_html"
	chown "${USERNAME}:www-data" "/home/${USERNAME}/public_html"
	chmod 750 "/home/${USERNAME}/public_html"

	# make sure included files permissions are correct
	chmod -f 700 "/home/${USERNAME}/.ssh"
	install_file "packages" "/home/${USERNAME}/.ssh/authorized_keys"
	chmod -f 600 "/home/${USERNAME}/.ssh/authorized_keys"
	# command="rrsync public_html" is already prepended in the authorized_keys files included in scipted_configuration container files
}

create_public_html "izzy"
create_public_html "ashimokawa"

systemctl reload lighttpd
