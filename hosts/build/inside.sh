#!/usr/bin/env bash

set -ex

source "base/users.sh"
source "base/base.sh"
setup_sshd

user_setup "build"
user_grant "build" "otto"
user_grant "build" "ashimokawa"
user_grant "build" "gusted"

install_file "build" "/home/build/.ssh/config"

curl -fsSL https://deb.nodesource.com/setup_16.x | bash -
apt-get install -y --no-install-recommends nodejs make librsvg2-bin imagemagick rsync
apt-get install -y gcc
apt-get install -y gettext --no-install-recommends

# do more stuff, or don't and use something else
