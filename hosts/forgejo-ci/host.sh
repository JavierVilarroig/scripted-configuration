#!/usr/bin/env bash

set -ex

MEM=40
CPU=18
LXC_CUSTOM=$(cat <<'EOF'
lxc.mount.entry = /dev/fuse dev/fuse none bind,create=file,rw 0 0
security.nesting = true
lxc.cap.drop =
EOF
)