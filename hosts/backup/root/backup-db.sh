#!/usr/bin/env bash

DAY="$(date +%A)"
mariadb-dump --single-transaction --quick gitea_production | gzip | ssh andreas-backup "cat > /codeberg-backup/kampenwand-database/${DAY}.sql.gz"
