#!/usr/bin/env bash

set -ex

source "base/users.sh"

# user_setup "otto" "sudo"
# user_setup "ashimokawa" "sudo"

source "base/base.sh"
setup_sshd

apt-get install -y git

# regular repo backup
source "base/systemd.sh"
systemd_timer "backup repos" "*-*-* *:05/15:00" "/root/repo-backup.git/main.sh" "root"
systemd_timer "cleanup repos" "Wed *-*-* 02:41:39" "/root/repo-backup.git/cleanup.sh" "root"
systemd_timer "reresolve wireguard address" "*-*-* *:00/5:00" "/usr/share/doc/wireguard-tools/examples/reresolve-dns/reresolve-dns.sh andreas" "root"

# database backup
install_file "backup" /root/backup-db.sh
systemd_timer "backup database" "*-*-* 03:28:17" "/root/backup-db.sh" "root"

# restic backup of btrfs
apt-get install -y restic
# ToDo: check if exists
#restic -r sftp:andreas-backup:/codeberg-backup/kampenwand_restic_btrfs init
