#!/usr/bin/env bash

set -ex

MEM=8
CPU=4
WEIGHT=10
LXC_CUSTOM=$(cat <<EOF
lxc.mount.entry = /dev/fuse dev/fuse none bind,create=file,rw 0 0
lxc.mount.entry = /mnt/ceph-cluster/production mnt/ceph-cluster none defaults,bind,create=dir 0 0
lxc.mount.entry = / mnt/btrfs none ro,defaults,bind,create=dir 0 0
security.nesting = true
EOF
)
